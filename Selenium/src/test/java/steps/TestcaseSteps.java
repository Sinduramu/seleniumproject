package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestcaseSteps {

	public ChromeDriver driver;

	@Given("Open the Chrome Browser")
	public void openTheChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@Given("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("Set the Timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("^Ener the username \"(.*)\"$")
	public void enerTheUsername(String uName) {
		driver.findElementById("username").sendKeys(uName);
	}

	@Given("Enter the Password as (.*)")
	public void enterThePassword(String pass) {
		driver.findElementById("password").sendKeys(pass);
	}

	@When("Click on Login button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@When("Click on CRMSFA icon")
	public void clickOnCRMSFAIcon() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@When("Click on Create Lead")
	public void clickOnCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@When("Enter the Firstname as (.*)")
	public void enterTheFirstname(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	}

	@When("Enter the Lastname as (.*)")
	public void enterTheLastname(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("Enter the CompanyName as (.*)")
	public void enterTheCompanyName(String cName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
	}

	@When("Click on Create Lead button")
	public void clickOnCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Lead should be created")
	public void leadShouldBeCreated() {
		System.out.println("Lead Created Sucessfully");
	}
	
	@When("Click on Find Lead sub menu")
	public void click_on_Find_Lead_sub_menu() {
		driver.findElementByLinkText("Find Leads").click();
	}
	
	@When("Search the Firstname as <fname>")
	public void search_the_Firstname(String fName) {
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fName);
	}

	@When("Click on Find Leads button")
	public void click_on_Find_Leads_button() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	}

	@When("Select the First record")
	public void select_the_First_record() {
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
	}

	@When("Click the Edit button")
	public void click_the_Edit_button() {
		driver.findElementByLinkText("Edit").click();
	}

	@When("Clear the Company Name")
	public void clear_the_Company_Name() {
		driver.findElementById("updateLeadForm_companyName").clear();
	}
	
	@When("Update the Company name as <cname>")
	public void update_the_Company_name_as_Cname(String cName) {
		driver.findElementById("updateLeadForm_companyName").sendKeys(cName);
	
	}

	

	@When("Click on Update button")
	public void click_on_Update_button() {
		driver.findElementByXPath("(//input[@class='smallSubmit'])[1]").click();
	}

	@Then("Lead should be updated successfully")
	public void lead_should_be_updated_successfully() {
	    System.out.println("Updated sucessfully");
	}


	
	
	}
