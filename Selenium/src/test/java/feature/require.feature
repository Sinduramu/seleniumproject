Feature: Create a Lead in Leaftaps application
Background:
Given Open the Chrome Browser 
And Maximize the Browser
And Set the Timeout
And Load the URL 
And Ener the username "DemoCSR"
And Enter the Password as crmsfa 
And Click on Login button
And Click on CRMSFA icon

Scenario Outline: TC001 Create a Lead

When Click on Create Lead 
And Enter the Firstname as <firstName>
And Enter the Lastname as <lastName>
And Enter the CompanyName as <CompanyName>
And Click on Create Lead button
Then Lead should be created

Examples:
|firstName|lastName|CompanyName|
|Sindu|Kalidas|Cognizant|
|Kalidas|Gurusamy|TCS|

Scenario Outline: TC002 Edit Lead

When Click on Find Lead sub menu
And Search the Firstname as <fname>
And Click on Find Leads button
And Select the First record
And Click the Edit button
And Clear the Company Name
And Update the Company name as <cname>
And Click on Update button
Then Lead should be updated successfully


Examples:
|fname|cname|
|Sindu|TCS	|








