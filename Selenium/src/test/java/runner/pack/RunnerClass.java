package runner.pack;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/java/feature/require.feature",glue="steps",monochrome=true)
//dryRun=true,snippets=SnippetType.CAMELCASE)
public class RunnerClass extends AbstractTestNGCucumberTests{

}
