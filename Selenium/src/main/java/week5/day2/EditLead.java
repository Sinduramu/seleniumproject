package week5.day2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class EditLead extends LearnAnnotation {
	@Test
	public void runEditLead() throws InterruptedException {
		// TODO Auto-generated method stub
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Sindu");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("TCS");
		String attribute1= driver.findElementById("updateLeadForm_companyName").getAttribute("value");
		driver.findElementByXPath("(//input[@class='smallSubmit'])[1]").click();
		String attribute2= driver.findElementById("viewLead_companyName_sp").getText();
		String[] splitOne=attribute2.split(" ");
		System.out.println(attribute1);
		System.out.println(attribute2);
		System.out.println(splitOne[0]);
		if (attribute1.equals(splitOne[0]))
			System.out.println("True");
		else
			System.out.println("false");
		
	}

}
