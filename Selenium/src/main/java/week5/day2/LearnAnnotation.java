package week5.day2;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class LearnAnnotation {
	public String excelFileName;
	public ChromeDriver driver;
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url, String uname, String pass)
	{

		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);                       
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pass);
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@AfterMethod
	public void close()
	{
		driver.close();
	}

	@DataProvider(name="fetchData")

	public String[][] createLead1() throws InvalidFormatException, IOException
	{
		return LearnExcel.readExcel(excelFileName);
		
	}



}
