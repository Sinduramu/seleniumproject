package week5.day2;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateLeadone extends LearnAnnotation{
	
	@BeforeTest
	public void setData()
	{
		excelFileName="CreateLeadData";
	}
	
	@Test(dataProvider="fetchData")
	public void runCreateLead(String cname, String fname, String lname)  {
		// TODO Auto-generated method stub
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
		driver.findElementByClassName("smallSubmit").click();
	
	}

}
