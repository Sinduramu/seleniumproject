package week5.day2;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static String [][] readExcel (String FileName) throws InvalidFormatException, IOException {
		// TODO Auto-generated method stub
 
		XSSFWorkbook workbook  = new XSSFWorkbook(new File("./TestData/"+FileName+".xlsx"));
		XSSFSheet sheet = workbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		int cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println();
		String[][] data=new String [rowCount][cellCount];
		for (int i=1; i<=rowCount; i++)
		{
			XSSFRow row = sheet.getRow(i);
			for (int j=0;j<cellCount;j++)
			{
				XSSFCell cell = row.getCell(j);
				data[i-1][j] = cell.getStringCellValue();
			}
		}
		workbook.close();
		return data;
		
	}

}
