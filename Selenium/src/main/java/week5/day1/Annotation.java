package week5.day1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotation {
  
	@Test(expectedExceptions=NumberFormatException.class)
  public void a() {
		String a="100A";
	  Integer.parseInt(a);
	
  }
/*	@Test(dependsOnMethods="a")
	  public void b() {
		  System.out.println("b");
	  }
	@Test
	  public void c() {
		  System.out.println("c");
	  }
	@Test
	  public void f() {
		  System.out.println("f");
	  }
	@Test
	  public void d() {
		  System.out.println("d");
	  }
	@Test
	  public void e() {
		  System.out.println("e");
	  }
		
	
*/	
/*  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("beforeMethod");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("afterMethod");
  }

  @BeforeClass
  public void beforeClass() {
	  System.out.println("beforeClass");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("afterClass");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("beforeTest");
  }
  

  @AfterTest
  public void afterTest() {
	  System.out.println("afterTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("beforeSuite");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("afterSuite");
  }

  
  
  @Test
  public void f1() {
	  System.out.println("f1");
  }
  @BeforeMethod
  public void beforeMethod1() {
	  System.out.println("beforeMethod1");
  }

  @AfterMethod
  public void afterMethod1() {
	  System.out.println("afterMethod1");
  }

  @BeforeClass
  public void beforeClass1() {
	  System.out.println("beforeClass1");
  }

  @AfterClass
  public void afterClass1() {
	  System.out.println("afterClass1");
  }

  @BeforeTest
  public void beforeTest1() {
	  System.out.println("beforeTest1");
  }
  

  @AfterTest
  public void afterTest1() {
	  System.out.println("afterTest1");
  }

  @BeforeSuite
  public void beforeSuite1() {
	  System.out.println("beforeSuite1");
  }

  @AfterSuite
  public void afterSuite1() {
	  System.out.println("afterSuite1");
  }

  
  
  
  @Test
  public void f2() {
	  System.out.println("test2");
  }
  @BeforeMethod
  public void beforeMethod2() {
	  System.out.println("beforeMethod2");
  }

  @AfterMethod
  public void afterMethod2() {
	  System.out.println("afterMethod2");
  }

  @BeforeClass
  public void beforeClass2() {
	  System.out.println("beforeClass2");
  }

  @AfterClass
  public void afterClass2() {
	  System.out.println("afterClass2");
  }

  @BeforeTest
  public void beforeTest2() {
	  System.out.println("beforeTest2");
  }
  

  @AfterTest
  public void afterTest2() {
	  System.out.println("afterTest2");
  }

  @BeforeSuite
  public void beforeSuite2() {
	  System.out.println("beforeSuite2");
  }

  @AfterSuite
  public void afterSuite2() {
	  System.out.println("afterSuite2");
*/ 
  }
