package week5.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class LearnAnnotation {
	public ChromeDriver driver;
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url, String uname, String pass)
	{

		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pass);
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@AfterMethod
	public void close()
	{
		driver.close();
	}

	@DataProvider

	public String[][] createLead1()
	{
		String [][] data = new String [2][3];
		data[0][0] = "Cognizant";
		data[0][1] = "Sindu";
		data[0][2]="Ramachandran";
		data[1][0] = "TCS";
		data[1][1] = "Kalidas";
		data[1][2]="Gurusamy";
		return data;
	}



}
