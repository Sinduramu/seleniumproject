package Week3.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<String>();
		list.add("Sindu");
		list.add("indu");
		list.add("Kali");
		list.add("Kanaga");
		list.add("Moshi");
		list.add("Ramu");
		
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
		Collections.reverse(list);
		System.out.println(list);
		System.out.println(list.get(1));
		Iterator <String> itr = list.iterator();
		while( itr.hasNext()) {
			System.out.print(itr.next()+" ");
		}
		//Print a Separate line

				for (String eachName : list) {
			System.out.println(eachName);
		}
		 
	
		ArrayList<String> list1 = new ArrayList<String>();
		list1.add("Kavi");
		list1.add("thara");
		System.out.println();
		System.out.println(list1);
		list.addAll(list1);
		System.out.println(list);
		list.addAll(2,list1);
		System.out.println(list);
		System.out.println(list.size());
		if(list.isEmpty()==false)
			System.out.println("List holds the value "+list);
		else
			System.out.println("List is empty");

		if(list.contains("indu"))
			System.out.println("Name is available");
		else
			System.out.println("Name is not available");

		if(list.containsAll(list1))
			System.out.println("All list1 names are available in list");
		else
			System.out.println("List1 names are not available in list");



	}
}

