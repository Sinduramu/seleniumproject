package Week3.day2;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class LearnSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<String> name = new LinkedHashSet<String>(); //Random Order
		name.add("Orange");
		name.add("Banana");
		name.add("Apple");
		name.add("chickoo");
		System.out.println(name);
		Set<String> name1 = new HashSet<String>(); //insertion order
		name1.addAll(name);
		System.out.println(name1);
		Set<String> name2 = new TreeSet<String>(); //ascii order
		name2.addAll(name);
		System.out.println(name2);
		
	}

}
