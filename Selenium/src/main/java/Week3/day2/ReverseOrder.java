package Week3.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReverseOrder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <String> test = new ArrayList <String>();
		test.add("TCS");
		test.add("HCL");
		test.add("CTS");
		test.add("Aspire Systems");
		test.add("Syntel");
		test.add("Infosys");
		System.out.println("Insertion Order"+ test);
		Collections.reverse(test);
		System.out.println("Reverse the Insertion Order"+ test);
		Collections.sort(test);
		System.out.println("Ascending Order"+ test);
		Collections.reverse(test);
		System.out.println("Descending Order"+ test);
	}

}
