package Week3.day2;



import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;




public class UniqueCharacter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String text="PayPal India";
		char[] charArray = text.toCharArray();
		//List<Character> test = new ArrayList<Character>();
		Map<Character,Integer> test=new LinkedHashMap<Character,Integer>();
		for (char d : charArray) {
				if(test.containsKey(d))
					test.put(d, test.get(d)+1);
				else
					test.put(d,1);		
			}
		for (Entry<Character,Integer> temp: test.entrySet())
			if((int)temp.getValue()==1)
			System.out.println(temp.getKey());
			
	
	}

}
