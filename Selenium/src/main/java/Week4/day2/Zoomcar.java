package Week4.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Zoomcar {
	static String text,replaceAll ;
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(ops);
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElementByXPath("//a[text()='Start your wonderful journey']").click();
		driver.findElementByXPath("//div[text()='Popular Pick-up points']/following::div[3]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//div[@class='days']/div[2]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//button[text()='Done']").click();
		Thread.sleep(2000);
		List<WebElement> price = driver.findElementsByXPath("//div[@class='price']");
		ArrayList<Integer> list=new ArrayList<Integer>();
		for (int i=0;i<price.size();i++)
		{
			text = price.get(i).getText();
			replaceAll = text.replaceAll("\\D","");
			int result =Integer.parseInt(replaceAll);
			list.add(result);

		}
		 Integer max = Collections.max(list);
		 System.out.println(max);
String text2 = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();

	System.out.println(text2);
	}

}
