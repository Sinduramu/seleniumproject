package Week4.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLEad {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List <String> list=new ArrayList<String>();
		list.addAll(windowHandles);
		WebDriver window1 = driver.switchTo().window(list.get(1));
		System.out.println(window1.getTitle());
		Thread.sleep(1000);
		driver.findElementByName("id").sendKeys("12640");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		Thread.sleep(1000);
		WebDriver window2 = driver.switchTo().window(list.get(0));
		System.out.println(window2.getTitle());
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Thread.sleep(1000);
		Set<String> windowHandles1 = driver.getWindowHandles();
		List <String> list1=new ArrayList<String>();
		list1.addAll(windowHandles1);
		WebDriver window3 = driver.switchTo().window(list1.get(1));
		System.out.println(window3.getTitle());
		Thread.sleep(1000);
		driver.findElementByName("id").sendKeys("12641");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		Thread.sleep(1000);
		WebDriver window4 = driver.switchTo().window(list1.get(0));
		System.out.println(window4.getTitle());
		driver.findElementByXPath("//a[text()='Merge']").click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys("12640");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String text = driver.findElementByXPath("//div[text()='No records to display']").getText();
		System.out.println(text);
		if (text.equals("No records to display"))
			System.out.println("Merged sucess");
		else
			System.out.println("non merged");
		
		
		driver.close();
		
		
	}

}
	