package Week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowsHandle {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List <String> list=new ArrayList<String>();
		list.addAll(windowHandles);
		WebDriver window1 = driver.switchTo().window(list.get(0));
		System.out.println(window1.getTitle());
		driver.close();
		WebDriver window2 = driver.switchTo().window(list.get(1));
		System.out.println(window2.getTitle());
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snap/image1.png");
		FileUtils.copyFile(source, dest);
		
		
		
	}

}
