package week1.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class WebLocator {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\HP\\Downloads\\Chromedriver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();

	//By ID is a unique locator - 1st preference

		driver.get("https://login.yahoo.com/");
		WebElement username=driver.findElement(By.id("login-username"));
		username.sendKeys("sinduramu@yahoo.com");
		WebElement button=driver.findElement(By.id("login-signin"));
		button.click();
		WebElement pwd=driver.findElement(By.id("login-passwd"));
		pwd.sendKeys("Kalidas.87");
		WebElement login=driver.findElement(By.id("login-signin"));
		login.click();


/*		2. By name is a unique locator - 1st preference

		driver.get("https://login.yahoo.com/");
		WebElement username=driver.findElement(By.name("username"));
		username.sendKeys("sinduramu@yahoo.com");
		WebElement button=driver.findElement(By.name("signin"));
		button.click();

		3. Xpath - 2nd preference

		driver.get("https://login.yahoo.com/");
		WebElement username=driver.findElement(By.xpath("//input[@id=\"login-username\"]"));
		username.sendKeys("sinduramu@yahoo.com");
		WebElement button=driver.findElement(By.xpath("//input[@id=\"login-signin\"]"));
		button.click();

		4 . cssSelector - 2nd preference

		driver.get("https://login.yahoo.com/");
		WebElement username=driver.findElement(By.cssSelector("#login-username"));
		username.sendKeys("sinduramu@yahoo.com");
		WebElement button=driver.findElement(By.cssSelector("#login-signin"));
		button.click();

		5. linkText - 4th preference
		driver.get("https://login.yahoo.com/");
		driver.findElement(By.linkText("Trouble signing in?")).click();

		6. Partial linkText (its not recommened) - 5th preference
		driver.get("https://login.yahoo.com/");
		driver.findElement(By.partialLinkText("Trouble")).click();

		7. Class name is not unique - 4th preference 	
		driver.get("https://login.yahoo.com/");
		driver.findElement(By.className("phone-no")).sendKeys("sinduramu@yahoo.com");
*/
	}
}
