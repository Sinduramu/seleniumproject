
//write a program to eliminate the character 'e' from the string "Welcome to Automation World."
package week1.day2;

public class RemoveCharacter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text="Welcome to Automation World.";
		String repl=text.replace("e", "");
		System.out.println(repl);	
	}

}
