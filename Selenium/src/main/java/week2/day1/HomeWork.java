package week2.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.Select;

public class HomeWork {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Sindu");
		driver.findElementById("userRegistrationForm:password").sendKeys("Kalidas_87");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Kalidas_87");
		WebElement dropDown1 = driver.findElementById("userRegistrationForm:securityQ");
		Select dd1=new Select(dropDown1);
		List<WebElement> options = dd1.getOptions();
		dd1.selectByIndex(options.size()-1);
		driver.findElementById("userRegistrationForm:password").sendKeys("College");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sindu");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("S");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Kalidas");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select dd2=new Select(day);
		dd2.selectByValue("10");
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select dd3=new Select(month);
		dd3.selectByVisibleText("JUL");
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dd4=new Select(year);
		dd4.selectByVisibleText("1987");
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select dd5=new Select(occupation);
		dd5.selectByVisibleText("Private");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("639423487744");
		driver.findElementById("userRegistrationForm:idno").sendKeys("DBKPS4422L");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select dd6=new Select(country);
		dd6.selectByVisibleText("India");
		Thread.sleep(2000);
		driver.findElementById("userRegistrationForm:email").sendKeys("sinduramu@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9940410427");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select dd7=new Select(nationality);
		dd7.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("323");
		driver.findElementById("userRegistrationForm:street").sendKeys("5th street");
		driver.findElementById("userRegistrationForm:area").sendKeys("Kasimedu");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600013",Keys.TAB);
		Thread.sleep(2000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select dd8=new Select(city);
		List<WebElement> options1 = dd8.getOptions();
		dd8.selectByIndex(options1.size()-1);
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select dd9=new Select(post);
		List<WebElement> optionss = dd9.getOptions();
		dd9.selectByIndex(optionss.size()-1);
		
		
	}
}
