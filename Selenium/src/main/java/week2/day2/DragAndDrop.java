package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://jqueryui.com/sortable/");
		// TODO Auto-generated method stub
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementByXPath("//ul[@class=\"ui-sortable\"]/li[1]");
		WebElement drop = driver.findElementByXPath("//ul[@class=\"ui-sortable\"]/li[4]");
		Point location = drop.getLocation();
		Actions builder =new Actions(driver);
		builder.dragAndDropBy(drag, 0, 130).perform();
		
		
	}

}
