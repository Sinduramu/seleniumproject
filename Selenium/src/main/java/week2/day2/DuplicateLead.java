package week2.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DEMOCSR");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Email").click();
		driver.findElementByName("emailAddress").sendKeys("sindu@gmail.com");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
		driver.findElementByLinkText("Duplicate Lead").click();
		String title = driver.getTitle();
		System.out.println(title);
		String att1 = driver.findElementById("createLeadForm_firstName").getAttribute("value");
		driver.findElementByClassName("smallSubmit").click();
		String att2 = driver.findElementById("viewLead_firstName_sp").getText();
		String[] splitOne = att2.split(" ");
		System.out.println(att1);
		System.out.println(splitOne[0]);
		if(att1.equals(splitOne[0]))
			System.out.println("True");
		else
			System.out.println("False");
		
		driver.close();

	}

}
