package week2.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateLead {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DEMOCSR");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sindu");
		String att1 = driver.findElementById("createLeadForm_firstName").getAttribute("value");
		driver.findElementById("createLeadForm_lastName").sendKeys("four");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Kalidas");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Ramu");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Profile");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select sel=new Select(source);
		sel.selectByVisibleText("Employee");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Profile Summary");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select sel1=new Select(industry);
		sel1.selectByIndex(3);
		WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select sel2=new Select(owner);
		sel2.selectByVisibleText("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_description").sendKeys("Extensively worked in different phases of Software Testing Life Cycle like Planning, Analysis, Test Design, Test Execution, and Defect Management.");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Familiar with all phases of Software Testing Life Cycle and administration of projects and have experience in Waterfall and Agile");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("1");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("044");
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select sel3=new Select(currency);
		sel3.selectByVisibleText("INR - Indian Rupee");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("1");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("yes");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("kali");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://leaftaps.com/crmsfa/control/createLead");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Kasimedu");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Royapuram");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select sel4=new Select(country);
		sel4.selectByVisibleText("India");
		Thread.sleep(5000);
		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select sel5=new Select(state);
		sel5.selectByVisibleText("TAMILNADU");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600013");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("600013");
		WebElement market= driver.findElementById("createLeadForm_marketingCampaignId");
		Select sel6=new Select(market);
		sel6.selectByIndex(1);
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9940410300");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("sindu@gmail.com");
		driver.findElementByClassName("smallSubmit").click();
		String att2 = driver.findElementById("viewLead_firstName_sp").getText();
		String[] splitOne = att2.split(" ");
		System.out.println(att1);
		System.out.println(splitOne[0]);
		if(att1.equals(splitOne[0]))
			System.out.println("True");
		else
			System.out.println("False");




	}

}
