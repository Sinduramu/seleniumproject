package testNGExamples;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class Annotation {

	public ChromeDriver driver;

	@Parameters ({"url","username","password"})
	@BeforeMethod
	public void learnAnnotation(String url, String uname, String pass)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pass);
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@DataProvider

	public String[][] create()
	{
		String[][] data = new String[2][3];
		data[0][0]="Sindu";
		data[0][1]="Kalidas";
		data[0][2]="Cognizant";
		data[1][0]="Kalidas";
		data[1][1]="Gurusamy";
		data[1][2]="TCS";
		return data;
	}


}